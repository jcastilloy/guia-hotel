module.exports = function(grunt) {
    grunt.initConfig({
        sass: {
          dist: {
            files: [{
              expand: true,
              cwd: 'css',
              src: ['*.scss'],
              dest: './css',
              ext: '.css'
            }]
          }
        },
        watch:{
          files: ['css/*.scss'],
          tasks: ['css']
        },
        browserSync: {
          dev: {
            bsFiles: { //browser files
                src: [
                  'css/*.css',
                  '*.html',
                  'js/*.js'
                ]
              },
            options: {
              watchTask: true,
              server:{
                baseDir: './'  //Directorio base para nuestro servidor
              }
            }
          }
        },
        imagemin: {
          dynamic: {
            files: [{
                expand: true,
                cwd: './',
                src: ['images/*.{png,jpg,gif}'],
                dest: 'dist/'
              }]
          }
        },
        copy: {
          html:{
            files:[{
              expand:true,
              dot: true,
              cwd: './',
              src:['*.html'],
              dest:'dist'
            }]
          },
          fonts: {
            files : [{
              expand: true,
              dot: true,
              cwd: 'node_modules/open-iconic/font',
              src: ['fonts/*.*'],
              dest: 'dist'
            }]
          }
        },
        clean: {
          build: {
            src: ['dist/']
          }
        },
        cssmin:{
          dist:{}
        },
        uglify: {
          dist:{}
        },
        filerev: {
          options:{
            encoding: 'utf8',
            algorithm: 'md5',
            length:20
          },
          release:{
            files:[{
              src:[
                'dist/js/*.js',
                'dist/css/*.css'
              ]
            }]
          }
        },
        concat: {
          options:{
            separator: ':'
          },
          dist:{}
        },
        useminPrepare: {
          foo: {
            dest: 'dist',
            src: ['index.html','about.html','precios.html','contacto.html']
          },
          options: {
            flow: {
              steps:{
                css: ['cssmin'],
                js: ['uglify']
              },
              post: {
                css: [{
                  name: 'cssmin',
                  createConfig: function(context, block){
                    var generated=context.options.generated;
                    generated.options={
                      keepSpecialComments:0,
                      rebase:false
                    }
                  }
                }]
              }
            }
          }
        },
        usemin: {
          html: ['dist/index.html','dist/about.html','dist/precios.html','dist/contacto.html'],
          options: {
            assetsdir: ['dist','dist/css','dist/js']
          }
        }
      });

    grunt.loadNpmTasks('grunt-contrib-watch'); //se mantiene observando los cambios que se hagan
    grunt.loadNpmTasks('grunt-contrib-sass'); //convierte de sass a css
    grunt.loadNpmTasks('grunt-browser-sync'); //sincroniza el explorador y lo actualiza cuando existen cambios 
    grunt.loadNpmTasks('grunt-contrib-imagemin'); //optimiza el tamaño de las imagenes
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-usemin');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-filerev');
    


    /** registro de tareas **/
    grunt.registerTask('css', ['sass']);
    grunt.registerTask('default',['browserSync', 'watch']);
    grunt.registerTask('img:compress', ['imagemin']);
    grunt.registerTask('build',[
      'clean',
      'copy',
      'imagemin',
      'useminPrepare',
      'concat',
      'cssmin',
      'uglify',
      'filerev',
      'usemin'
    ]);

};