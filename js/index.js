
$(function () {
    
    $('[data-toggle="tooltip"]').tooltip();
    
    $('[data-toggle="popover"]').popover();
    
    $('.carousel').carousel({
        interval: 2000
    });

    $('#reservaModal').on('shown.bs.modal', function(e){
        $('#btnBarcelona').removeClass('btn-primary');
        $('#btnBarcelona').addClass('btn-dark');
        $('#btnBarcelona').prop('disabled', true);
    });

    $('#reservaModal').on('hidden.bs.modal', function(e){
        $('#btnBarcelona').removeClass('btn-dark');
        $('#btnBarcelona').addClass('btn-primary');
        $('#btnBarcelona').prop('disabled', false)
    });
    
});